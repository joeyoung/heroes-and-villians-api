$host.ui.RawUI.WindowTitle ="HAV Development"

Invoke-Expression "dotnet build"

$Env:ASPNETCORE_ENVIRONMENT = "Development"
Set-Location "HAV.Api"
Invoke-Expression "dotnet run"
