[CmdletBinding()]
param(
    [Parameter(Mandatory=$False, Position=1)]
    [switch] $migrate
)

$location = Get-Location

$Env:ASPNETCORE_ENVIRONMENT = "TESTING"

if ($migrate) {
    Set-Location "HAV.Domain"

    Invoke-Expression "dotnet ef database update --startup-project ..\HAV.Api\HAV.Api.csproj"

    Set-Location $location
}

Set-Location "HAV.Api.Tests"

Invoke-Expression "dotnet test"

Set-Location $location