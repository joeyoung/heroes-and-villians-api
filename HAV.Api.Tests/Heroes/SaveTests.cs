namespace HAV.Api.Tests.Heroes
{
    using System.Threading.Tasks;
    using HAV.Api.Heroes;
    using HAV.Domain.Heroes;
    using Shouldly;
    using Xunit;

    using static DbContextFixture;

    public class SaveTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_save()
        {
            // arrange
            var ironMan = new HeroEntity 
            {
                Name = "Iron Man",
                City = "New York",
                CoverName = "Tony Stark"
            };

            await InsertAsync(ironMan);

            // act
            var command = new Save.Command
            {
                Id = ironMan.Id,
                Name = ironMan.Name,
                CoverName = ironMan.CoverName,
                City = "Philadelphia"
            };

            await SendAsync(command);

            // assert        
            var updated = await FindAsync<HeroEntity>(ironMan.Id);

            updated.City.ShouldBe(command.City);
        }
    }
}