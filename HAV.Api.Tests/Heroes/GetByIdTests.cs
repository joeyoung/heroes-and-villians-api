namespace HAV.Api.Tests.Heroes
{
    using System.Threading.Tasks;
    using Xunit;
    using Shouldly;
    using HAV.Api.Heroes;
    using HAV.Domain.Heroes;
    using Respawn;
    using System;
    using Xunit.Abstractions;

    using static DbContextFixture;


    public class GetByIdTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_return_hero_by_id()
        {
            var ironMan = new HeroEntity 
            {
                Name = "Iron Man",
                City = "New York",
                CoverName = "Tony Stark"
            };

            await InsertAsync(ironMan);

            var result = await SendAsync(new GetById.Query {Id = ironMan.Id});

            result.ShouldNotBeNull();
            result.Name.ShouldBe(ironMan.Name);
        }
    }
}