namespace HAV.Api.Tests.Heroes
{
    using System.Threading.Tasks;
    using Xunit;
    using Shouldly;
    using HAV.Api.Heroes;
    using HAV.Domain.Heroes;
    using Respawn;
    using System;
    using Xunit.Abstractions;

    using static DbContextFixture;


    public class GetAllTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_return_all_heroes()
        {
            var ironMan = new HeroEntity 
            {
                Name = "Iron Man",
                City = "New York",
                CoverName = "Tony Stark"
            };

            var captainAmerica = new HeroEntity
            {
                Name = "Captain America",
                City = "Boston",
                CoverName = "Steve Rogers"
            };

            await InsertAsync(ironMan, captainAmerica);

            var result = await SendAsync(new GetAll.Query());

            result.ShouldNotBeNull();
            result.Heroes.Count.ShouldBe(2);
        }
    }
}