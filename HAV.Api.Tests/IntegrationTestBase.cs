﻿namespace HAV.Api.Tests
{
    using System.Threading.Tasks;
    using MediatR;
    using Xunit;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public abstract class IntegrationTestBase : IAsyncLifetime
    {
        public virtual async Task InitializeAsync()
        {
            await DbContextFixture.ResetCheckpoint();
        }

        public virtual Task DisposeAsync() => Task.CompletedTask;

        public static Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            return DbContextFixture.ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }

        public static Task SendAsync(IRequest request)
        {
            return DbContextFixture.ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }
    }
}