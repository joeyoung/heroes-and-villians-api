namespace HAV.Api.Tests.Villains
{
    using System.Threading.Tasks;
    using Xunit;
    using Shouldly;
    using HAV.Api.Villains;
    using HAV.Domain.Villains;
    using Respawn;
    using System;
    using Xunit.Abstractions;

    using static DbContextFixture;


    public class GetByIdTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_return_villain_by_id()
        {
            var joker = new VillainEntity 
            {
                Name = "Joker",
                City = "Gotham",
            };

            await InsertAsync(joker);

            var result = await SendAsync(new GetById.Query {Id = joker.Id});

            result.ShouldNotBeNull();
            result.Name.ShouldBe(joker.Name);
        }
    }
}