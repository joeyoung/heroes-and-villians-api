namespace HAV.Api.Tests.Villains
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using HAV.Api.Villains;
    using Microsoft.EntityFrameworkCore;
    using Respawn;
    using Shouldly;
    using Xunit;
    using static DbContextFixture;

    public class CreateTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_create_new_hero()
        {
            // arrange 
            var command = new Create.Command
            {
                Name = "Joker",
                City = "Gotham",
            };

            // act
            var id = await SendAsync(command);
            
            // assert
            var created = await ExecuteDbContextAsync(db => db.Villains.Where(x => x.Id == id)
                .SingleOrDefaultAsync());

            created.Name.ShouldBe(command.Name);       
            created.City.ShouldBe(command.City);

            created.CreatedOn.ShouldNotBeNull();
        }
    }
}