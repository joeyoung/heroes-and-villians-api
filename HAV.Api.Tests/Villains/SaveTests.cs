namespace HAV.Api.Tests.Villains
{
    using System.Threading.Tasks;
    using HAV.Api.Villains;
    using HAV.Domain.Villains;
    using Shouldly;
    using Xunit;

    using static DbContextFixture;

    public class SaveTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_save()
        {
            // arrange
            var joker = new VillainEntity
            {
                Name = "Joker",
                City = "Gotham",
            };

            await InsertAsync(joker);

            // act
            var command = new Save.Command
            {
                Id = joker.Id,
                Name = joker.Name,
                City = "New York"
            };

            await SendAsync(command);

            // assert        
            var updated = await FindAsync<VillainEntity>(joker.Id);

            updated.City.ShouldBe(command.City);
        }
    }
}