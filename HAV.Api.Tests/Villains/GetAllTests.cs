namespace HAV.Api.Tests.Villains
{
    using System.Threading.Tasks;
    using Xunit;
    using Shouldly;
    using HAV.Api.Villains;
    using HAV.Domain.Villains;
    using Respawn;
    using System;
    using Xunit.Abstractions;

    using static DbContextFixture;


    public class GetAllTests : IntegrationTestBase
    {
        [Fact]
        public async Task Should_return_all_Villains()
        {
            var joker = new VillainEntity 
            {
                Name = "Joker",
                City = "Gotham"
            };

            var poisonIvy = new VillainEntity
            {
                Name = "Poison Ivy",
                City = "Gotham",
            };

            await InsertAsync(joker, poisonIvy);

            var result = await SendAsync(new GetAll.Query());

            result.ShouldNotBeNull();
            result.Villains.Count.ShouldBe(2);
        }
    }
}