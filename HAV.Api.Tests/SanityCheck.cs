using System;
using Xunit;

namespace HAV.Api.Web.Tests
{
    public class SanityCheck
    {
        [Fact]
        public void AllGood()
        {
            Assert.True(1 + 1 == 2);
        }
    }
}
