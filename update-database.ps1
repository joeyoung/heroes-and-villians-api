# execute like the following
# .\update-database

[CmdletBinding()]
param(
    [Parameter(Mandatory=$False, Position=2)]
    [String] $environment = "Development"
)

$location = Get-Location

Set-Location "HAV.Domain"

$Env:ASPNETCORE_ENVIRONMENT = $environment

Invoke-Expression "dotnet ef database update --startup-project ..\HAV.Api\HAV.Api.csproj"

Set-Location $location