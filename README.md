# README #

Heroes and Villains API in .NET Core

### What ###

Simple API for Heroes and Villains done in .NET Core

### Uses
* ASP.NET Core WebApi
* Entity Framework Core
* Mediatr
* Automapper
* Respawn