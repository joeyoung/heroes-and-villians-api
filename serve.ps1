[CmdletBinding()]
Param(
    [Parameter(Mandatory=$False, Position=1)]
    [string] $env = "DEV"
)

switch ($env.ToUpper())
{
    "DEV" { Start-Process powershell .\run-dev.ps1 }
    default { "Unknown Environment! Supported Environments are DEV" }
}
