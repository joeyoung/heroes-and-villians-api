# execute like the following
# .\add-migration CreateMyTable

[CmdletBinding()]
param(
    [Parameter(Mandatory=$True, Position=1)]
    [String] $name,
    [Parameter(Mandatory=$False, Position=2)]
    [String] $environment = "Development"
)

$location = Get-Location

Set-Location "HAV.Domain"

$Env:ASPNETCORE_ENVIRONMENT = $environment

Invoke-Expression "dotnet ef migrations add $name --startup-project ..\HAV.Api\HAV.Api.csproj"

Set-Location $location