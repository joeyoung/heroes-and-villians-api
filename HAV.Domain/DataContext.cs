namespace HAV.Domain
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using HAV.Domain.Heroes;
    using HAV.Domain.Villains;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;

    public class DataContext : DbContext
    {
        private IDbContextTransaction currentTransaction;

        public DbSet<HeroEntity> Heroes {get; set;}
        public DbSet<VillainEntity> Villains {get; set;}

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<HeroEntity>(x => {
                x.ToTable("Heroes");
                x.HasKey(k => k.Id);
            });

            builder.Entity<VillainEntity>(x => {
                x.ToTable("Villains");
                x.HasKey(k => k.Id);
            });
        }

        public override int SaveChanges()
        {
            SetAuditFields();
            return base.SaveChanges();
        }       

        public async Task BeginTransactionAsync()
        {
            if (currentTransaction != null)
            {
                return;
            }

            currentTransaction = await Database.BeginTransactionAsync();
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync();
                currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
            }
            finally
            {
                if (currentTransaction != null)
                {
                    currentTransaction.Dispose();
                    currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                currentTransaction?.Rollback();
            }
            finally
            {
                if (currentTransaction != null)
                {
                    currentTransaction.Dispose();
                    currentTransaction = null;
                }
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            SetAuditFields();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void SetAuditFields()
        {
            var auditableEntities = ChangeTracker.Entries<AuditableEntity>()
                .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));

            var now = DateTime.Now;

            foreach (var entry in auditableEntities)
            {
                var auditable = entry.Entity as AuditableEntity;
                if (auditable != null)
                {
                    if (entry.State == EntityState.Added)
                    {
                        auditable.CreatedBy = "";
                        auditable.CreatedOn = now;
                    }

                    if (entry.State == EntityState.Modified)
                    {
                        auditable.UpdatedBy = "";
                        auditable.UpdatedOn = now;
                    }
                }
            }
            
        }
    }
}