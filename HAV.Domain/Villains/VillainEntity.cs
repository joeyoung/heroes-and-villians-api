namespace HAV.Domain.Villains
{
    public class VillainEntity : AuditableEntity
    {
        public string Name { get; set; }   
        public string City { get; set; }
    }
}