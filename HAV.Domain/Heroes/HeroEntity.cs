using System.ComponentModel.DataAnnotations.Schema;

namespace HAV.Domain.Heroes
{
    public class HeroEntity : AuditableEntity
    {
        public string Name { get; set; }   
        public string City { get; set; }
        public string CoverName { get; set; }   
    }
}