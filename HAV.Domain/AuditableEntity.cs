namespace HAV.Domain
{
    using System;

    public abstract class AuditableEntity : Entity
    {
        public DateTime CreatedOn {get; set;}
        public string CreatedBy {get; set; }
        public DateTime? UpdatedOn {get; set;}
        public string UpdatedBy {get; set;}
    }
}