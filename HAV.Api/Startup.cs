﻿namespace HAV.Api
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using HAV.Api.Filters;
    using MediatR;
    using AutoMapper;
    using HAV.Domain.Heroes;
    using HAV.Domain.Villains;
    using HAV.Domain;
    using Microsoft.EntityFrameworkCore;
    using FluentValidation.AspNetCore;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
           
            services.AddMediatR();
            services.AddAutoMapper();

            services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString));

            services.AddMvc(opt => {
                opt.Filters.Add(typeof(TransactionFilter));
                opt.Filters.Add(typeof(ValidationFilter));
            })
            .AddFluentValidation(cfg => cfg.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => {
                    builder.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    
                });
                Mapper.AssertConfigurationIsValid();
            }

            app.UseMvc();
        }
    }
}
