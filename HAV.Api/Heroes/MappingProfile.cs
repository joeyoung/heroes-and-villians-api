namespace HAV.Api.Heroes
{
    using AutoMapper;
    using HAV.Domain.Heroes;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // document -> model
            CreateMap<HeroEntity, GetAll.Model>();
            CreateMap<HeroEntity, GetById.Model>();

            // model -> entity
            CreateMap<Create.Command, HeroEntity>(MemberList.Source);
            CreateMap<Save.Command, HeroEntity>(MemberList.Source);
        }
    }
}