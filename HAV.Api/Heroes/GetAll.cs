namespace HAV.Api.Heroes
{
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper.QueryableExtensions;
    using MediatR;
    using HAV.Domain;
    using AutoMapper;
    using HAV.Domain.Heroes;
    using Microsoft.EntityFrameworkCore;

    public class GetAll
    {
        public class Model 
        {
            public string Id { get; set; }    
            public string Name {get; set;}
            public string City {get; set;}
            public string CoverName {get; set;}
        }

        public class Query : IRequest<QueryResult>
        {
           
        }

        public class QueryResult
        {
            public List<Model> Heroes { get; set; }
        }
        public class QueryHandler : AsyncRequestHandler<Query, QueryResult>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public QueryHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }
            
            protected override async Task<QueryResult> HandleCore(Query message)
            {
                var heroes = await dataContext.Heroes.ToListAsync();
                return new QueryResult {Heroes = mapper.Map<List<Model>>(heroes) };
            }
        }
    }
}