namespace HAV.Api.Heroes
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using HAV.Api;
    using MediatR;

    [ApiVersion("1.0")]
    [Produces("applicaiton/json")]
    [Route("api/[controller]")]
    public class HeroesController : Controller 
    {
        private readonly IMediator mediator;
        
        public HeroesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet()]
        public async Task<IActionResult> Get() 
        {
            var model = await mediator.Send(new GetAll.Query());
            return Json(model.Heroes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) 
        {
            var model = await mediator.Send(new GetById.Query() {Id = id });
            return Json(model);
        }
        
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Update(int id, [FromBody] Save.Command command)
        {
            await mediator.Send(command);
            return Ok();
        }

        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Create([FromBody] Create.Command command)
        {
            var id = await mediator.Send(command);
            return Json(id);
        }
    }
}