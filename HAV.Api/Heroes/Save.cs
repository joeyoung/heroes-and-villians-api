namespace HAV.Api.Heroes
{
    using System.Threading.Tasks;
    using AutoMapper;
    using MediatR;
    using HAV.Domain;
    using HAV.Domain.Heroes;

    public class Save
    {
        public class Command : IRequest
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
            public string CoverName {get; set;}
        }

        public class CommandHandler : AsyncRequestHandler<Command>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public CommandHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }
                        
            protected override async Task HandleCore(Command message)
            {
                var hero = await dataContext.Heroes.FindAsync(message.Id);
                if (hero != null)
                {
                    mapper.Map<Command, HeroEntity>(message, hero);
                }
            }
        }
    }

}