namespace HAV.Api.Filters
{
    using System.Threading.Tasks;
    using HAV.Domain;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class TransactionFilter : IAsyncActionFilter
    {
        private readonly DataContext dataContext;

        public TransactionFilter(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            try
            {
                await dataContext.BeginTransactionAsync();
                var action = await next();
                if (action.Exception != null && !action.ExceptionHandled)
                {
                    dataContext.RollbackTransaction();
                }
                else
                {
                    await dataContext.CommitTransactionAsync();
                }
            }
            catch
            {
                    dataContext.RollbackTransaction();
                    throw;
            }

        }
    }
}