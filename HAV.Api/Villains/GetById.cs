namespace HAV.Api.Villains
{
    using System.Threading.Tasks;
    using MediatR;
    using HAV.Domain;
    using AutoMapper;
    using HAV.Domain.Villains;

    public class GetById 
    {
        public class Model 
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string City { get; set;}
        }

        public class Query : IRequest<Model>
        {
            public int Id { get; set; }
        }

        public class QueryHandler : AsyncRequestHandler<Query, Model>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public QueryHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }

            protected override async Task<Model> HandleCore(Query message)
            {
                var villain = await dataContext.Villains.FindAsync(message.Id);

                return mapper.Map<Model>(villain);
            }
        }
    }
}