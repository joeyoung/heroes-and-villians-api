namespace HAV.Api.Villains
{
    using AutoMapper;
    using HAV.Domain.Villains;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // document -> model
            CreateMap<VillainEntity, GetAll.Model>();
            CreateMap<VillainEntity, GetById.Model>();

            // model -> entity
            CreateMap<Create.Command, VillainEntity>(MemberList.Source);
            CreateMap<Save.Command, VillainEntity>(MemberList.Source);
        }
    }
}