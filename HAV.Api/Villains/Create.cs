namespace HAV.Api.Villains
{
    using System.Threading.Tasks;
    using AutoMapper;
    using MediatR;
    using HAV.Domain;
    using HAV.Domain.Villains;

    public class Create
    {
        public class Command : IRequest<int>
        {
            public string Name { get; set; }

            public string City { get; set; }
        }

        public class CommandHandler : AsyncRequestHandler<Command, int>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public CommandHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }
            protected override async Task<int> HandleCore(Command message)
            {
                var villain = mapper.Map<Command, VillainEntity>(message);

                dataContext.Add(villain);

                await dataContext.SaveChangesAsync();

                return villain.Id;
            }
        }
    }
}