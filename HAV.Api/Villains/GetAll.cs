namespace HAV.Api.Villains
{
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper.QueryableExtensions;
    using MediatR;
    using HAV.Domain;
    using AutoMapper;
    using HAV.Domain.Villains;
    using Microsoft.EntityFrameworkCore;

    public class GetAll
    {
        public class Model 
        {
            public string Id { get; set; }    
            public string Name {get; set;}
            public string City {get; set;}
        }

        public class Query : IRequest<QueryResult>
        {
           
        }

        public class QueryResult
        {
            public List<Model> Villains { get; set; }
        }
        public class QueryHandler : AsyncRequestHandler<Query, QueryResult>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public QueryHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }
            
            protected override async Task<QueryResult> HandleCore(Query message)
            {
                var villains = await dataContext.Villains.ToListAsync();
                return new QueryResult {Villains = mapper.Map<List<Model>>(villains) };
            }
        }
    }
}