namespace HAV.Api.Villains
{
    using System.Threading.Tasks;
    using AutoMapper;
    using MediatR;
    using HAV.Domain;
    using HAV.Domain.Villains;

    public class Save
    {
        public class Command : IRequest
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
        }

        public class CommandHandler : AsyncRequestHandler<Command>
        {
            private readonly DataContext dataContext;
            private readonly IMapper mapper;

            public CommandHandler(DataContext dataContext, IMapper mapper)
            {
                this.dataContext = dataContext;
                this.mapper = mapper;
            }
                        
            protected override async Task HandleCore(Command message)
            {
                var villain = await dataContext.Villains.FindAsync(message.Id);
                if (villain != null)
                {
                    mapper.Map<Command, VillainEntity>(message, villain);

                    await dataContext.SaveChangesAsync();
                }
            }
        }
    }

}