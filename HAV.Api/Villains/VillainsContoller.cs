namespace HAV.Api.Villains
{
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;

    [ApiVersion("1.0")]
    [Produces("applicaiton/json")]
    [Route("api/controller")]
    public class VillansController : Controller
    {
        private readonly IMediator mediator;

        public VillansController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet()]
        public async Task<IActionResult> Get() 
        {
            var model = await mediator.Send(new GetAll.Query());
            return Json(model.Villains);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) 
        {
            var model = await mediator.Send(new GetById.Query() {Id = id });
            return Json(model);
        }
        
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Update(int id, [FromBody] Save.Command command)
        {
            await mediator.Send(command);
            return Ok();
        }

        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Create([FromBody] Create.Command command)
        {
            var id = await mediator.Send(command);
            return Json(id);
        }
    }
}